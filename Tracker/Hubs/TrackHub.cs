﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tracker.Models;

namespace Tracker.Hubs
{

    public class TrackHub : Hub
    {
        public async Task TrackMe(Gibier Bambi)
        {
            await Clients.AllExcept(Context.ConnectionId).SendAsync("UpdateTrack", Bambi);
        }
    }
}
