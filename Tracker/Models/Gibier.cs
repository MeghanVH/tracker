﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tracker.Models
{
    public class Gibier
    {
        private string _nom;
        private decimal _lat, _long;
        private int _id;

        public string Nom
        {
            get
            {
                return _nom;
            }

            set
            {
                _nom = value;
            }
        }

        public decimal Lat
        {
            get
            {
                return _lat;
            }

            set
            {
                _lat = value;
            }
        }

        public decimal Long
        {
            get
            {
                return _long;
            }

            set
            {
                _long = value;
            }
        }

        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }
    }
}

